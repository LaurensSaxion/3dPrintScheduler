package nl.saxion.Databases;


import nl.saxion.Models.Printer;
import nl.saxion.Models.Spool;
import nl.saxion.PrinterManager;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class PrinterDatabase {
    private final ArrayList<Printer> printers;
    private final PrinterManager manager;

    public PrinterDatabase(PrinterManager manager) {
        this.manager = manager;
        this.printers = readPrintersFromFile();
    }


    private ArrayList readPrintersFromFile() {
        ArrayList<Printer> printers = new ArrayList<>();

        //read printers from JSON
        JSONParser jsonParser = new JSONParser();
        URL printersResource = getClass().getResource("/printers.json");
        if (printersResource == null) {
            System.err.println("Warning: Could not find printers.json file");
            return null;
        }
        try (FileReader reader = new FileReader(printersResource.getFile())) {
            JSONArray printersJSON = (JSONArray) jsonParser.parse(reader);
            for (Object p : printersJSON) {
                JSONObject printer = (JSONObject) p;
                int id = ((Long) printer.get("id")).intValue();
                int type = ((Long) printer.get("type")).intValue();
                String name = (String) printer.get("name");
                String manufacturer = (String) printer.get("manufacturer");
                int maxX = ((Long) printer.get("maxX")).intValue();
                int maxY = ((Long) printer.get("maxY")).intValue();
                int maxZ = ((Long) printer.get("maxZ")).intValue();
                int maxColors = ((Long) printer.get("maxColors")).intValue();


                //check if the printer is housed or not
                boolean housed;
                if (type == 2 || type == 4) {
                    housed = true;
                } else {
                    housed = false;
                }

                //current Spool
                JSONArray currentSpools = (JSONArray) printer.get("currentSpools");
                Spool[] spools = new Spool[currentSpools.size()];

                //set spools
                    for (int i = 0; i < spools.length; i++) {
                        spools[i] = manager.findSpool(((Long) currentSpools.get(i)).intValue());
                        if (spools[i] != null) {
                            spools[i].setInUse(true);
                        }
                    }

                printers.add(new Printer(id, name, manufacturer, maxX, maxY, maxZ, housed, maxColors, spools));

            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return printers;
    }

    public ArrayList<Printer> getPrinters() {
        return printers;
    }

    public Printer findPrinter(String printerName) {
        Printer printer = null;

        for (Printer printer1 : printers) {
            if (printerName.equals(printer1.toString())) {
                printer = printer1;
            }
        }

        return printer;
    }
}
