package nl.saxion.Databases;

import nl.saxion.Models.FilamentType;
import nl.saxion.Models.Spool;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

public class SpoolDatabase {
    private final ArrayList<Spool> spools;

    public SpoolDatabase() {
        this.spools = readSpoolsFromFile();
    }

    private ArrayList readSpoolsFromFile() {
        ArrayList<Spool> spools = new ArrayList<>();

        //read spools from CSV
        try {
            File spoolsCSV = new File("src/main/resources/spools.csv");
            Scanner csvReader = new Scanner((spoolsCSV));
            csvReader.nextLine();
            while (csvReader.hasNextLine()) {
                String line = csvReader.nextLine();
                String[] parsedLine = line.split(",");

                //read spool properties from parsed lines
                int id = Integer.parseInt(parsedLine[0]);
                String color = parsedLine[1];
                String filamentType = parsedLine[2];
                double length = Double.parseDouble(parsedLine[3]);

                //determine filamentType from String
                FilamentType type;
                switch (filamentType) {
                    case "PLA":
                        type = FilamentType.PLA;
                        break;
                    case "PETG":
                        type = FilamentType.PETG;
                        break;
                    case "ABS":
                        type = FilamentType.ABS;
                        break;
                    default:
                        System.out.println("Not a valid filamentType, bailing out");
                        return null;
                }

                //add spool to database
                spools.add(new Spool(id, color, type, length));
           }
        } catch (FileNotFoundException e) {
            System.err.println(e);
        }
        return spools;
    }

    public Spool findSpool(int id) {
        for (Spool spool : spools) {
            if (spool.getId() == id) {
                return spool;
            }
        }
        return null;
    }

    public ArrayList<Spool> getSpools() {
        return spools;
    }

    public ArrayList<Spool> getFreeSpools() {
        ArrayList<Spool> freeSpools = new ArrayList<>();

        for (Spool spool : spools) {
            if (!spool.isInUse()) {
                freeSpools.add(spool);
            }
        }

        return freeSpools;
    }

    public ArrayList<String> getAvailableColors(String filamentType) {
        ArrayList<String> availableColors = new ArrayList<>();
        for (Spool spool : spools) {
            if (filamentType == spool.getFilamentType() && !availableColors.contains(spool.getColor())) {
                availableColors.add(spool.getColor());
            }

        }
        return availableColors;
    }
}
