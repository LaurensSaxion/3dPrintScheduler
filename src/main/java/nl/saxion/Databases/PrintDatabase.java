package nl.saxion.Databases;

import nl.saxion.Models.Print;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class PrintDatabase {
    private final ArrayList<Print> prints;

    public PrintDatabase() {
        this.prints = readPrintsFromFile();
    }

    private ArrayList readPrintsFromFile() {
        ArrayList<Print> prints = new ArrayList<>();

        //read prints from JSON
        JSONParser jsonParser = new JSONParser();
        URL printResource = getClass().getResource("/prints.json");
        if (printResource == null) {
            System.err.println("Warning: Could not find prints.json file");
            return null;
        }
        try (FileReader reader = new FileReader(printResource.getFile())) {
            JSONArray printsJSON = (JSONArray) jsonParser.parse(reader);
            for (Object p : printsJSON) {
                JSONObject print = (JSONObject) p;
                String name = (String) print.get("name");
                String filename = (String) print.get("filename");
                int height = ((Long) print.get("height")).intValue();
                int width = ((Long) print.get("width")).intValue();
                int length = ((Long) print.get("length")).intValue();
                JSONArray fLength = (JSONArray) print.get("filamentLength");
                ArrayList<Integer> filamentLength = new ArrayList();
                for (int i = 0; i < fLength.size(); i++) {
                    filamentLength.add(((Long) fLength.get(i)).intValue());
                }
                prints.add(new Print(name, filename, height, width, length, filamentLength));
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }


        return prints;
    }

    public Print findPrint(String printName) {
        for (Print print : prints) {
            if (print.getName().equals(printName)) {
                return print;
            }
        }
        return null;
    }

    public int getAmountOfColors(String printName) {
        return findPrint(printName).getFilamentLength().size();
    }

    public ArrayList<Print> getPrints() {
        return prints;
    }
}
