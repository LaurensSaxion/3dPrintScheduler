package nl.saxion.PrintStrategy;


import nl.saxion.Models.PrintTask;
import nl.saxion.Models.Printer;
import nl.saxion.Models.Spool;
import nl.saxion.PrinterManager;

import java.util.ArrayList;

public class LessSpoolChanges implements PrintStrategy{

    private final PrinterManager manager;

    public LessSpoolChanges(PrinterManager manager) {
        this.manager = manager;
    }

    @Override
    public void selectPrintTask(Printer printer) {
        // First we look if there's a task that matches the current spool on the printer.
        for (PrintTask printTask : manager.getPendingPrintTasks()) {
            if (printer.printCheck(printTask)) { //if printCheck returns true, the task is assigned to the printer and removed from the queue.
                manager.addRunningTask(printer, printTask);
                manager.getPendingPrintTasks().remove(printTask);
                System.out.println("Started task " + printTask + " on printer " + printer.getName());
                return;
            }
        }

        // If we didn't find a print for the current spool we search for a print with the free spools.
        for (PrintTask printTask : manager.getPendingPrintTasks()) {
            String filamentType = printTask.getFilamentType();
            ArrayList<Spool> chosenSpools = new ArrayList<>();

            for (int i = 0; i < printTask.getColors().size(); i++) {
                for (Spool spool : manager.getFreeSpools()) {
                    if (spool.spoolMatch(printTask.getColors().get(i), filamentType) && !manager.containsSpool(chosenSpools, printTask.getColors().get(i))) {
                        chosenSpools.add(spool);
                    }
                }
            }
            //if the required spools are available we will run printCheck again with the found set of spools to swap out
            if (printer.printCheckWithSpoolSwap(printTask, chosenSpools) && chosenSpools.size() == printTask.getColors().size()) {
                manager.addRunningTask(printer, printTask);
                manager.getPendingPrintTasks().remove(printTask);
                System.out.println("Started task " + printTask + " on printer " + printer.getName());
                return;
            }
        }
    }
}
