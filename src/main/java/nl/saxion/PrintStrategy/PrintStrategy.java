package nl.saxion.PrintStrategy;

import nl.saxion.Models.Printer;

public interface PrintStrategy {

    void selectPrintTask(Printer printer);
}
