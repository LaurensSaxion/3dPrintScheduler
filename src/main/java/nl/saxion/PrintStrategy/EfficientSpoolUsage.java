package nl.saxion.PrintStrategy;


import nl.saxion.Models.PrintTask;
import nl.saxion.Models.Printer;
import nl.saxion.Models.Spool;
import nl.saxion.PrinterManager;

import java.util.ArrayList;

public class EfficientSpoolUsage implements PrintStrategy{
    private final PrinterManager manager;

    public EfficientSpoolUsage(PrinterManager manager) {

        this.manager = manager;
    }

    @Override
    public void selectPrintTask(Printer printer) {
        while (manager.getPendingPrintTasks().size() > 0) {
            // First select the task with the biggest print
            PrintTask biggestPrint = null;
            for (PrintTask printTask : manager.getPendingPrintTasks()) {
                if (biggestPrint == null) {
                    biggestPrint = printTask;
                } else if (printTask.getPrint().getLength() > biggestPrint.getPrint().getLength()) {
                    biggestPrint = printTask;
                }
            }

            //then select the smallest possible spool available
            String filamentType = biggestPrint.getFilamentType();
            ArrayList<Spool> chosenSpools = new ArrayList<>();

            for (int i = 0; i < biggestPrint.getColors().size(); i++) {
                Spool smallestSpool = null;
                for (Spool spool : manager.getFreeSpools()) {
                    if (spool.spoolMatch(biggestPrint.getColors().get(i), filamentType) && !manager.containsSpool(chosenSpools, biggestPrint.getColors().get(i)) && smallestSpool == null) {
                        smallestSpool = spool;
                    } else if (spool.spoolMatch(biggestPrint.getColors().get(i), filamentType) && !manager.containsSpool(chosenSpools, biggestPrint.getColors().get(i)) && spool.getLength() < smallestSpool.getLength()) {
                        smallestSpool = spool;
                    }
                }
                chosenSpools.add(smallestSpool);
            }

            //if the required spools are available we will run printCheck again with the found set of spools to swap out
            if (printer.printCheckWithSpoolSwap(biggestPrint, chosenSpools) && chosenSpools.size() == biggestPrint.getColors().size()) {
                manager.addRunningTask(printer, biggestPrint);
                manager.getPendingPrintTasks().remove(biggestPrint);
                System.out.println("Started task " + biggestPrint + " on printer " + printer.getName());
                return;
            }
        }
    }
}
