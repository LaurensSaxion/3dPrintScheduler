package nl.saxion;

import nl.saxion.Databases.PrintDatabase;
import nl.saxion.Databases.PrinterDatabase;
import nl.saxion.Databases.SpoolDatabase;
import nl.saxion.Models.*;
import nl.saxion.PrintStrategy.EfficientSpoolUsage;
import nl.saxion.PrintStrategy.LessSpoolChanges;
import nl.saxion.PrintStrategy.PrintStrategy;

import java.util.*;

public class PrinterManager{
    private final SpoolDatabase spoolDB = new SpoolDatabase();
    private final PrinterDatabase printerDB = new PrinterDatabase(this);
    private final PrintDatabase printDB = new PrintDatabase();

    private final ArrayList<PrintTask> pendingPrintTasks = new ArrayList<>();
    private final HashMap<Printer, PrintTask> runningPrintTasks = new HashMap();
    private PrintStrategy printStrategy = new LessSpoolChanges(this);


    public boolean containsSpool(final List<Spool> list, final String name) {
        return list.stream().anyMatch(o -> o.getColor().equals(name));
    }

    public void selectPrintTask(Printer printer) {
       printStrategy.selectPrintTask(printer);
    }

    public void startInitialQueue() {
        for (int i = 0; i < printerDB.getPrinters().size(); i++) {
            selectPrintTask(printerDB.getPrinters().get(i));
        }
    }

    public ArrayList<String> getPrints() {
        ArrayList<Print> prints = printDB.getPrints();
        ArrayList<String> printNames = new ArrayList<>();

        for (Print print : prints) {
            printNames.add(print.getName());
        }

        return printNames;
    }

    public String getPrintsString() {
        StringBuilder printList = new StringBuilder();
        ArrayList<Print> prints = printDB.getPrints();

        for (int i = 0; i < prints.size(); i++) {
            printList.append(prints.get(i));
            if (!(i == (prints.size() - 1))) {
                printList.append(System.lineSeparator());
            }
        }
        return printList.toString();
    }

    public ArrayList<String> getPrinters() {
        ArrayList<String> printers = new ArrayList<>();

        for (Printer printer : printerDB.getPrinters()) {
            printers.add(printer.toString());
        }
        return printers;
    }

    public String getPrinterCurrentTask(String printer, boolean detailedInfo) {
        Printer printer1 = printerDB.findPrinter(printer);
        if (!runningPrintTasks.containsKey(printer1)) {
            return null;
        }
        if (detailedInfo) {
            return printer1.getId() + ": " + printer1.getName() + " - " + runningPrintTasks.get(printer1).toString();
        } else {
            return runningPrintTasks.get(printer1).toString();
        }
    }

    public ArrayList<PrintTask> getPendingPrintTasks() {
        return pendingPrintTasks;
    }

    public ArrayList<String> getPendingPrintTasksToString() {
        ArrayList<String> printTasks = new ArrayList<>();
        for (PrintTask printTask : pendingPrintTasks) {
            printTasks.add(printTask.toString());
        }
        return printTasks;
    }

    public void addRunningTask(Printer printer, PrintTask printTask) {
        runningPrintTasks.put(printer, printTask);
    }

    public void addPrintTask(String printName, List<String> colors, String type) {

        Print print = printDB.findPrint(printName);
        if (print == null) {
            printError("Could not find print with name " + printName);
            return;
        }
        if (colors.size() == 0) {
            printError("Need at least one color, but none given");
            return;
        }
        for (String color : colors) {
            boolean found = false;
            for (Spool spool : spoolDB.getSpools()) {
                if (spool.getColor().equals(color) && spool.getFilamentType() == type) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                printError("Color " + color + " (" + type + ") not found");
                return;
            }
        }

        PrintTask task = new PrintTask(print, colors, type.toString());
        pendingPrintTasks.add(task);
        System.out.println("Added task to queue");

    }

    public int getTotalPrintColors(String printName) {
        return printDB.getAmountOfColors(printName);
    }

    public ArrayList<Spool> getFreeSpools() {
        return spoolDB.getFreeSpools();
    }

    public Spool findSpool(int id) {
        return spoolDB.findSpool(id);
    }

    public String getSpoolsString() {
        StringBuilder spoolList = new StringBuilder();
        ArrayList<Spool> spools = spoolDB.getSpools();

        for (int i = 0; i < spools.size(); i++) {
            spoolList.append(spools.get(i));
            if (!(i == (spools.size() - 1))) {
                spoolList.append(System.lineSeparator());
            }
        }
        return spoolList.toString();
    }

    public ArrayList<String> getAvailableColors(String filamentType) {
        return spoolDB.getAvailableColors(filamentType);
    }

    public void changePrintStrategy(int strategyChoice) {
        if (strategyChoice == 1) {
            printStrategy = new LessSpoolChanges(this);
        } else if (strategyChoice == 2) {
            printStrategy = new EfficientSpoolUsage(this);
        }
    }

    public void registerCompletion(int printerId, boolean status) {
        Map.Entry<Printer, PrintTask> foundEntry = null;
        for (Map.Entry<Printer, PrintTask> entry : runningPrintTasks.entrySet()) {
            if (entry.getKey().getId() == printerId) {
                foundEntry = entry;
                break;
            }
        }
        if (foundEntry == null) {
            printError("cannot find a running task on printer with ID " + printerId);
            return;
        }
        PrintTask task = foundEntry.getValue();
        //check if the print was succesful
        if (!status) {
            pendingPrintTasks.add(task); // if not, add the task back to the queue.
        }
        runningPrintTasks.remove(foundEntry.getKey());

        System.out.println("Task " + task + " removed from printer "
                + foundEntry.getKey().getName());

        //reduce length of spool which was used for the print
        Printer printer = foundEntry.getKey();
        Spool[] spools = printer.getCurrentSpools();
        for (int i = 0; i < spools.length && i < task.getColors().size(); i++) {
            spools[i].reduceLength(task.getPrint().getFilamentLength().get(i));
        }
        selectPrintTask(printer);
    }

    private void printError(String s) {
        System.out.println("Error: " + s);
        System.out.println("Press Enter to continue");
        new Scanner(System.in).nextLine();
    }


}
