package nl.saxion;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Main {
    Scanner scanner = new Scanner(System.in);
    private final PrinterManager manager = new PrinterManager();

    private String printStrategy = "Less Spool Changes";

    public static void main(String[] args) {
        new Main().run();
    }

    public void run() {
        int choice = 1;
        while (choice > 0 && choice < 10) {
            menu();
            choice = menuChoice(9);
            switch (choice) {
                case 1:
                    addNewPrintTask();
                    break;
                case 2:
                    registerPrinterStatus(true);
                    break;
                case 3:
                    registerPrinterStatus(false);
                    break;
                case 4:
                    changePrintStrategy();
                    break;
                case 5:
                    startPrintQueue();
                    break;
                case 6:
                    showPrints();
                    break;
                case 7:
                    showPrinters();
                    break;
                case 8:
                    showSpools();
                    break;
                case 9:
                    showPendingPrintTasks();
                    break;
            }
        }
        exit();


        
    }

    public void menu() {
        System.out.println("Print Manager");
        System.out.println("=============");
        System.out.println("1) Add new Print Task");
        System.out.println("2) Register Printer Completion");
        System.out.println("3) Register Printer Failure");
        System.out.println("4) Change printing style");
        System.out.println("5) Start Print Queue");
        System.out.println("6) Show prints");
        System.out.println("7) Show printers");
        System.out.println("8) Show spools");
        System.out.println("9) Show pending print tasks");
        System.out.println("0) Exit");
    }

    private void startPrintQueue() {
        manager.startInitialQueue();
    }

    private void exit() {

    }

    // This method only changes the name but does not actually work.
    // It exists to demonstrate the output.
    // in the future strategy might be added.
    private void changePrintStrategy() {
        System.out.println("Current strategy: " + printStrategy);
        System.out.println("1: Less Spool Changes");
        System.out.println("2: Efficient Spool Usage");
        System.out.println("Choose strategy: ");
        int strategyChoice = numberInput(1, 2);
        manager.changePrintStrategy(strategyChoice);
        if (strategyChoice == 1) {
            printStrategy = "Less Spool Changes";
        } else if (strategyChoice == 2) {
            printStrategy = "Efficient Spool Usage";
        }
    }

    // TODO: This should be based on which printer is finished printing.
    private void registerPrinterStatus(boolean status) {
        ArrayList<String> printers = manager.getPrinters();
        System.out.println("---------- Currently Running Printers ----------");
        for (String printer : printers) {
            String printerCurrentTask = manager.getPrinterCurrentTask(printer, true);
            if (printerCurrentTask != null) {
                System.out.println(printerCurrentTask);
            }
        }
        int printerId = numberInput(1, printers.size());
        if (status) {
            System.out.print("Printer that is done (ID): ");
            manager.registerCompletion(printerId, true);
        } else {
            System.out.print("Printer ID that failed: ");
            manager.registerCompletion(printerId, false);
        }

    }

    private void addNewPrintTask() {
        List<String> colors = new ArrayList<>();
        ArrayList<String> prints = manager.getPrints();
        System.out.println("---------- Available prints ----------");
        for (int i = 0; i < prints.size(); i++) {
            System.out.println((i + 1) + ": " + prints.get(i));
        }
        System.out.println("--------------------------------------");
        System.out.print("Print number: ");
        int printNumber = numberInput(1, prints.size());
        String printName = prints.get(printNumber-1);

        //choose filament type
        System.out.println("---------- Filament Type ----------");
        System.out.println("1: PLA");
        System.out.println("2: PETG");
        System.out.println("3: ABS");
        System.out.print("Filament type number: ");
        int ftype = numberInput(1, 3);
        String type;
        switch (ftype) {
            case 1:
                type = "PLA";
                break;
            case 2:
                type = "PETG";
                break;
            case 3:
                type = "ABS";
                break;
            default:
                System.out.println("Not a valid filamentType, bailing out");
                return;
        }

        //choose colors
        System.out.println("---------- Spools ----------");
        ArrayList<String> availableColors = manager.getAvailableColors(type);
        for (int i = 0; i < availableColors.size(); i++) {
            System.out.println((i + 1) + ": " + availableColors.get(i) + " (" + type + ")");
        }

        System.out.println("----------------------------");


        int colorChoice = 0;
        for (int i = 0; i < manager.getTotalPrintColors(printName); i++) {
            System.out.print("Color number: ");
            colorChoice = numberInput(1, availableColors.size());
            colors.add(availableColors.get(colorChoice-1));
        }

        //add printTask to queue
        manager.addPrintTask(printName, colors, type);
    }

    private void showPrints() {
        System.out.println("---------- Available prints ----------");
        System.out.println(manager.getPrintsString());
        System.out.println("--------------------------------------");
    }

    private void showSpools() {
        System.out.println("---------- Spools ----------");
        System.out.println(manager.getSpoolsString());
        System.out.println("----------------------------");
    }

    private void showPrinters() {
        ArrayList<String> printers = manager.getPrinters();
        System.out.println("--------- Available printers ---------");
        for (String printer : printers) {
            System.out.print(printer);
            String currentTask = manager.getPrinterCurrentTask(printer, false);
            if (currentTask != null) {
                System.out.println("Current Print Task: " + currentTask);
            }
            System.out.println();
        }
        System.out.println("--------------------------------------");
    }

    private void showPendingPrintTasks() {
        ArrayList<String> printTasks = manager.getPendingPrintTasksToString();
        System.out.println("--------- Pending Print Tasks ---------");
        for (String task : printTasks) {
            System.out.println(task);
        }
        System.out.println("--------------------------------------");
    }


    public int menuChoice(int max) {
        int choice = -1;
        while (choice < 0 || choice > max) {
            System.out.print("Choose an option: ");
            try {
                choice = scanner.nextInt();
            } catch (InputMismatchException e) {
                //try again after consuming the current line
                System.out.println("Error: Invalid input");
                scanner.nextLine();
            }
        }
        return choice;
    }

    public int numberInput() {
        int input = scanner.nextInt();
        return input;
    }

    public int numberInput(int min, int max) {
        int input = numberInput();
        while (input < min || input > max) {
            input = numberInput();
        }
        return input;
    }
}
