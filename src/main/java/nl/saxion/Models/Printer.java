package nl.saxion.Models;


import java.util.ArrayList;

public class Printer {
    private final int id;
    private final String name;
    private final String manufacturer;
    private final int maxX;
    private final int maxY;
    private final int maxZ;
    private final Boolean housed;
    private final int maxColors;
    private final Spool[] spools;


    public Printer(int id, String name, String manufacturer, int maxX, int maxY, int maxZ, Boolean housed, int maxColors, Spool[] spools) {
        this.id = id;
        this.name = name;
        this.manufacturer = manufacturer;
        this.maxX = maxX;
        this.maxY = maxY;
        this.maxZ = maxZ;
        this.housed = housed;
        this.maxColors = maxColors;
        this.spools = spools;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Spool[] getCurrentSpools() {
        return spools;
    }

    public void setCurrentSpools(ArrayList<Spool> newSpools) {
        for (int i = 0; i < newSpools.size(); i++) {
            this.spools[i] = newSpools.get(i);
        }
    }

    public boolean printFits(Print print) {
        return print.getHeight() <= maxZ && print.getWidth() <= maxX && print.getLength() <= maxY;
    }

    public boolean printCheck(PrintTask task) {
        return printCheckWithSpoolSwap(task, null);
    }

    public boolean printCheckWithSpoolSwap(PrintTask task, ArrayList<Spool> chosenSpools) {
        if (!printFits(task.getPrint())) {
            return false; //if the print is too big for the printer, return false
        }
        if (task.getFilamentType().equals("ABS") && housed == false) {
            return false; //if the filamentType is ABS and the printer is not housed, return false
        }
        if (task.getColors().size() > 1 && maxColors <= 1) {
            return false; //if the print consists of more than one color but the printer is single color, return false
        }
        if (chosenSpools != null) { //if a new set of spools is provided, swap them with the old spools
            swapSpools(chosenSpools);
        }

        //check if the printer's current spools match the print
        for (int i = 0; i < spools.length && i < task.getColors().size(); i++) {
            if (spools[i] == null && chosenSpools == null) {
                return false;
            }
            if (spools[i] != null) {
                if (!spools[i].spoolMatch(task.getColors().get(i), task.getFilamentType()) && chosenSpools == null) {
                    return false; //if the spools do not match, return false
                }
            }
        }

        //if all conditions are met, return true
        return true;
    }

    public void swapSpools(ArrayList<Spool> newSpools) {

        for (int i = 0; i < spools.length; i++) {
            if (spools[i] != null) {
                spools[i].setInUse(false);
            }

        }
        for (int i = 0; i < newSpools.size(); i++) {
            System.out.println("Please place spool " + newSpools.get(i).getId() + " in printer " + getName());
            newSpools.get(i).setInUse(true);
        }
        setCurrentSpools(newSpools);
    }

    @Override
    public String toString() {
        String result = "ID: " + id + System.lineSeparator() +
                "Name: " + name + System.lineSeparator() +
                "Manufacturer: " + manufacturer + System.lineSeparator() +
                "maxX: " + maxX + System.lineSeparator() +
                "maxY: " + maxY + System.lineSeparator() +
                "maxZ: " + maxZ + System.lineSeparator();
        if (spools[0] != null) {
            result += "Current spool: " + spools[0].getId() + System.lineSeparator();
        } else {
            result += "Current spool: " + -1 + System.lineSeparator();
        }
        if (maxColors > 1) {
            result += "maxColors: " + maxColors + System.lineSeparator();
            for (int i = 1; i < spools.length; i++) {
                if (spools[i] != null) {
                    result += "spool" + (i + 1) + ": " + spools[i].getId() + System.lineSeparator();
                } else {
                    result += "spool" + (i + 1) + ": " + -1 + System.lineSeparator();
                }

            }
        }
        return result;
    }
}
