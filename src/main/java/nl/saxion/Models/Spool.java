package nl.saxion.Models;

public class Spool {
    private final int id;
    private final String color;
    private final FilamentType filamentType;
    private double length;
    private boolean inUse = false;

    public Spool(int id, String color, FilamentType filamentType, double length) {
        this.id = id;
        this.color = color;
        this.filamentType = filamentType;
        this.length = length;
    }

    public int getId() {
        return this.id;
    }

    public double getLength() {
        return length;
    }

    public boolean isInUse() {
        return inUse;
    }

    public void setInUse(boolean inUse) {
        this.inUse = inUse;
    }

    public boolean spoolMatch(String color, String filamentType) {
        FilamentType type = FilamentType.valueOf(filamentType);
        return color.equals(this.color) && type == FilamentType.valueOf(getFilamentType());
    }

    /**
     * This method will try to reduce the length of the spool.
     *
     * @param byLength
     * @return boolean which tells you if it is possible or not.
     */
    public boolean reduceLength(double byLength) {
        boolean success = true;
        this.length -= byLength;
        if (this.length < 0) {
            this.length -= byLength;
            success = false;
        }
        return success;
    }

    public String getColor() {
        return color;
    }

    public String getFilamentType() {
        return filamentType.toString();
    }

    @Override
    public String toString() {
        return "===== Spool " + id + " =====" + System.lineSeparator() +
                "color: " + color + System.lineSeparator() +
                "filamentType: " + filamentType + System.lineSeparator() +
                "length: " + length + System.lineSeparator();
    }
}
