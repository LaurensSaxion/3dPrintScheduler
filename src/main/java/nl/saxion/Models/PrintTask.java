package nl.saxion.Models;

import java.util.List;

public class PrintTask {
    private final Print print;
    private final List<String> colors;
    private final String filamentType;


    public PrintTask(Print print, List<String> colors, String filamentType) {
        this.print = print;
        this.colors = colors;
        this.filamentType = filamentType;

    }

    public List<String> getColors() {
        return colors;
    }

    public String getFilamentType() {
        return filamentType;
    }

    public Print getPrint() {
        return print;
    }

    @Override
    public String toString() {
        return print.getName() + " " + filamentType + " " + colors.toString();
    }
}
